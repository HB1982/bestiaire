<?php

add_theme_support('title-tag');

add_theme_support('post-thumbnails');



function enregistre_mon_menu()
{
    register_nav_menu('menu_principal', __('Menu principal'));
}
add_action('init', 'enregistre_mon_menu');

if (function_exists('acf_add_local_field_group')) :

    acf_add_local_field_group(array(
        'key' => 'group_6227360e60139',
        'title' => 'informations animaux',
        'fields' => array(
            array(
                'key' => 'field_62273647ce5cd',
                'label' => 'Taille',
                'name' => 'Taille',
                'type' => 'number',
                'instructions' => 'sélectionnez un nombre',
                'required' => 0,
                'conditional_logic' => 0,
                'wrapper' => array(
                    'width' => '',
                    'class' => '',
                    'id' => '',
                ),
                'default_value' => 0,
                'placeholder' => 'rentrez la taille',
                'prepend' => 'taille:',
                'append' => 'Toise(s)',
                'min' => 0,
                'max' => '',
                'step' => '0.1',
            ),
            array(
                'key' => 'field_6227674df7405',
                'label' => 'nourriture',
                'name' => 'nourriture',
                'type' => 'relationship',
                'instructions' => '',
                'required' => 0,
                'conditional_logic' => 0,
                'wrapper' => array(
                    'width' => '',
                    'class' => '',
                    'id' => '',
                ),
                'post_type' => '',
                'taxonomy' => '',
                'filters' => array(
                    0 => 'search',
                    1 => 'post_type',
                    2 => 'taxonomy',
                ),
                'elements' => '',
                'min' => '',
                'max' => '',
                'return_format' => 'object',
            ),
        ),
        'location' => array(
            array(
                array(
                    'param' => 'post_type',
                    'operator' => '==',
                    'value' => 'post',
                ),
            ),
        ),
        'menu_order' => 0,
        'position' => 'normal',
        'style' => 'default',
        'label_placement' => 'top',
        'instruction_placement' => 'field',
        'hide_on_screen' => '',
        'active' => true,
        'description' => '',
        'show_in_rest' => 0,
    ));

endif;
