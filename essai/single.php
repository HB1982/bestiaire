<?php get_header(); ?>

<article class="container">

    <div class="singlecard">

        <div class="left">

            <h1 class='animal'><?php the_title(); ?></h1>

            <?php if (get_field('Taille')) : ?>
                <h2 class='titre'>Taille:</h2>
                <p><?php the_field('Taille'); ?> Toise(s)</p>
            <?php endif; ?>

            <h2 class='titre'> Description:</h2><?php the_content(); ?>
        </div>

        <div class="droite">
            <?php the_post_thumbnail('post-thumbnail', ['class' => 'img', 'alt' => 'image']) ?>

            <?php $nourriture = get_field('nourriture');

            if ($nourriture) : ?><h2 class='titre'>Mange:</h2>

                <ul class='ul'>

                    <?php foreach ($nourriture as $post) :


                        setup_postdata($post); ?>

                        <li>
                            <a class="nourriture" href="<?php the_permalink(); ?>"><?php the_title(); ?></a>

                        </li>
                    <?php endforeach; ?>

                </ul>

                <?php

                wp_reset_postdata(); ?>

            <?php endif; ?>
        </div>


    </div>

</article>

<?php get_footer(); ?>